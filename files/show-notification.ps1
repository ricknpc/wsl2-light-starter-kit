param (
    [string]$title,
    [Parameter(Mandatory=$true)][string]$text,
    [string]$icon = "info"
)

[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

$tip = New-Object System.Windows.Forms.NotifyIcon

if ($icon -eq "error") {
    $tip.Icon = [System.Drawing.SystemIcons]::Error
    $tip.BalloonTipIcon = "Error"
} elseif ($icon -eq "warning") {
    $tip.Icon = [System.Drawing.SystemIcons]::Warning
    $tip.BalloonTipIcon = "Warning"
} else {
    $tip.Icon = [System.Drawing.SystemIcons]::Information
    $tip.BalloonTipIcon = "Info"
}
$tip.BalloonTipTitle = $title
$tip.BalloonTipText = $text
$tip.Visible = $True

$tip.ShowBalloonTip(10000)
