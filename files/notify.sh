#!/usr/bin/env bash

psExe='/mnt/c/Windows/System32/WindowsPowerShell/v1.0/powershell.exe'
ps1Path='C:\scripts\show-notification.ps1'
icon="$1"
title="$2"
text="$3"

if [[ -z "$title" ]] || [[ ! "$icon" =~ ^(info|warning|error)$ ]]; then
    echo 'Usage: notify <icon> <title> [text]'
    echo '    icon    one of: "info", "warning", or "error"'
    echo '    title   main message for notification'
    echo '    text    optionally provide additional info below title'
    exit 1
fi

# winforms requires text param, but treats it as title if no title provided
if [[ -z "$text" ]]; then
    text="$title"
    title=""
fi

"$psExe" -ExecutionPolicy Unrestricted -file "$ps1Path" -title "$title" -text "$text" -icon "$icon"
