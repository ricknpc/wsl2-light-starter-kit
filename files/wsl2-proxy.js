const net = require('net');
const exec = require('child_process').exec;
const args = process.argv.slice(2);
const flags = { verbose: false, help: false };
let numberOfConnections = 0;



// pull flags (anything starting with -) from argv first
for (let i = 0; i < args.length; i++) {
    let arg = args[i];
    if (!arg.startsWith('-')) continue;

    arg = arg.toLowerCase();
    if (arg === '-v' || arg === '--verbose') flags.verbose = true;
    else if (arg === '-h' || arg === '--help') flags.help = true;

    args.splice(i--, 1);
}

// print usage if help flagged, or wrong number of non-flag args, or non-flag arg doesn't match
if (flags.help || args.length !== 1 || !args[0].match(/^[0-9,]+$/)) {
    console.log('\nUsage: node wsl2-proxy.js [options] <ports>');
    console.log('    <ports>         comma-separated e.g. "80,443"');
    console.log('    -v, --verbose   log extra details about traffic');
    console.log('    -h, --help      show this help');
    process.exit(1);
}
const ports = args[0].split(',').map(p => parseInt(p));



// main execution - get ip address of wsl from within wsl, then start the port proxies
exec('wsl hostname -I', (err, stdout, stderr) => {
    if (err) {
        console.error(red('Failed to find WSL2 IP address:\n'), err);
        process.exit(2);
    }

    const match = stdout.trim().match(/^([0-9]{0,3}\.){3}([0-9]{0,3})/);
    if (!match) {
        console.error(red('Failed to find WS2 IP address, output was:\n'), stdout);
        process.exit(2);
    }

    const wslAddr = match[0];
    console.log(cyan(`Proxying ports ${ports.join(', ')} to address ${wslAddr}`));

    for (let port of ports) {
        startPortProxy(wslAddr, port);
    }
});



// convenience functions for bash colours
function red(str) {
    return '\033[1;31m' + str + '\033[0m';
}
function green(str) {
    return '\033[1;32m' + str + '\033[0m';
}
function yellow(str) {
    return '\033[1;33m' + str + '\033[0m';
}
function cyan(str) {
    return '\033[1;36m' + str + '\033[0m';
}



// proxy on addr and port
function startPortProxy(addr, port) {
    const server = net.createServer();

    server.on('listening', () => {
        console.log(green(`Listening on port ${port}`));
    });
    server.on('error', (err) => {
        console.error(red(`Connection error on port ${port}:\n`), err);
    });

    // when a connection comes in
    server.on('connection', (inSocket) => {

        // connection numbers are just for tracking, keep them low digits and therefore easy to read
        const conNum = numberOfConnections++;
        if (numberOfConnections > 999) numberOfConnections = 0;

        let inSockClosed = false;
        let wslSockClosed = false;

        // create connection to wsl2
        if (flags.verbose) console.log(cyan(`New connection (#${conNum}) on port ${port} from addr ${inSocket.remoteAddress}`));
        const wslSocket = net.createConnection({ host: addr, port: port });


        // log errors, some less severe
        inSocket.on('error', (err) => {
            const msg = `${conNum}: Incoming socket error: ${err.code} - ${err.message}`;
            if (err.code === 'EPIPE' || err.code === 'ECONNRESET') console.error(yellow(msg));
            else console.error(red(msg));
        });
        // close wsl socket if incoming socket is closed
        inSocket.on('close', () => {
            if (flags.verbose) console.log(`${conNum}: Incoming socket closed`);
            inSockClosed = true;
            if (!wslSockClosed) wslSocket.end();
        });
        // write incoming tcp data to wsl socket
        inSocket.on('data', (chunk) => {
            if (flags.verbose) console.log(`${conNum}: Sending incoming data to client`);
            if (!wslSockClosed) wslSocket.write(chunk);
        });

        // log errors, some less severe
        wslSocket.on('error', (err) => {
            const msg = `${conNum}: Client socket error: ${err.code} - ${err.message}`;
            if (err.code === 'EPIPE' || err.code === 'ECONNRESET') console.error(yellow(msg));
            else console.error(red(msg));
        });
        // close incoming socket if wsl socket is closed
        wslSocket.on('close', () => {
            if (flags.verbose) console.log(`${conNum}: Client socket closed`);
            wslSockClosed = true;
            if (!inSockClosed) inSocket.end();
        });
        // write tcp data returned from wsl socket back to incoming connection
        wslSocket.on('data', (chunk) => {
            if (flags.verbose) console.log(`${conNum}: Sending client data back to incoming`);
            if (!inSockClosed) inSocket.write(chunk);
        });

    });

    server.listen(port);
}
