#!/usr/bin/env bash

# script to make sure ssh-agent is running and known to the current instance of bash
ensure-ssh-agent() {
    local sock_dir='/tmp/ssh-agent'
    local sock_path="$sock_dir/$USER.agent.sock"
    mkdir -p "$sock_dir"

    local agent_id=$(ps aux | awk '/ssh-agent\s.*'$USER'/ {print $2}')
    local extra_agents_found=$(echo -n "$agent_id" | wc -l)

    if [ "$extra_agents_found" -gt 0 ]; then
        echo -e '\033[1;31mToo many ssh-agent processes running, please kill any extras then re-run `ensure-ssh-agent`\033[0m'
        return 1
    elif [ -n "$agent_id" ]; then
        if [ -S "$sock_path" ]; then
            export SSH_AGENT_PID="$agent_id"
            export SSH_AUTH_SOCK="$sock_path"
        else
            echo -e '\033[1;31mFound ssh-agent but no socket path, please kill ssh-agents and re-run `ensure-ssh-agent`\033[0m'
            return 1
        fi
    else
        if [ -S "$sock_path" ]; then
            rm -f "$sock_path"
        fi
        local output=$(ssh-agent -a "$sock_path")
        export SSH_AGENT_PID=$(echo "$output" | sed -nE 's/SSH_AGENT_PID=([0-9]+).*/\1/p')
        export SSH_AUTH_SOCK="$sock_path"
    fi
}
ensure-ssh-agent

# easily notify after a long-running command
# use like: `./command.sh arg1 arg2; nac 'optional text'`
nac() {
    if [[ $? == 0 ]]; then
        notify info 'Command Successful' "$*"
    else
        notify error 'Command Failed' "$*"
    fi
}

# alias some useful windows paths (note: explorer returns exit code 1 for some reason, hence the ; true)
alias vscode='/mnt/c/Program\ Files/Microsoft\ VS\ Code/bin/code'
alias copy='/mnt/c/Windows/System32/clip.exe'
alias explore='/mnt/c/Windows/SysWOW64/explorer.exe .; true'

# wsl2 starts in windows home folder, move to linux home folder instead, but keep a reference
if [[ "$PWD" =~ ^/mnt/c/Users/[^/]+/?$ ]]; then
    echo "$PWD" > ~/.windows-home
    cd ~
fi
if [[ -f ~/.windows-home ]]; then
    export WINDOWS_HOME=$(cat ~/.windows-home)
fi
