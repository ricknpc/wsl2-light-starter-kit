#!/usr/bin/env bash

# test file existence least likely to be modified, to see if this script has been run before
firstRun() {
    [ ! -f /etc/sudoers.d/passwordless-startup ]
}

# find dir of this script
setupDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# apt installs only for first run
if firstRun; then
    # install very basic linux necessities
    sudo apt update
    sudo apt upgrade
    sudo apt install apt-transport-https ca-certificates curl gnupg-agent gnupg gnupg2 software-properties-common

    # install some extra optional but helpful packages
    sudo apt install build-essential net-tools unzip jq
fi

# copy wsl.conf file
sudo cp "$setupDir/files/wsl.conf" /etc/wsl.conf

# copy startup script, only once in case user would like to add to it
if [ ! -f /usr/local/bin/startup ]; then
    sudo cp "$setupDir/files/startup.sh" /usr/local/bin/startup
    sudo chown root:root /usr/local/bin/startup
    sudo chmod 0754 /usr/local/bin/startup
fi

# allow passwordless sudo for startup script
if firstRun; then
    echo "$USER ALL=(ALL:ALL) NOPASSWD: /usr/local/bin/startup" | sudo tee /etc/sudoers.d/passwordless-startup
fi

# copy notification powershell script to windows, and bash script to local bin
mkdir -p /mnt/c/scripts
cp "$setupDir/files/show-notification.ps1" /mnt/c/scripts/show-notification.ps1
sudo cp "$setupDir/files/notify.sh" /usr/local/bin/notify &&\
    sudo chown root:root /usr/local/bin/notify &&\
    sudo chmod 0755 /usr/local/bin/notify

# copy and enable the shell extras
cp "$setupDir/files/wsl.rc.sh" ~/.wsl.shrc
if [ -z "$(grep '\.wsl\.shrc' ~/.bashrc)" ]; then
    echo -e '\n# load WSL2 shell extras\n. ~/.wsl.shrc' >> ~/.bashrc
fi
