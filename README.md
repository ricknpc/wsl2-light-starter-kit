# WSL2 Light Starter Kit

## What is WSL2?

WSL2 is the second version of [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/about) - a way to run a damn-near-full Linux environment on your Windows machine. It's essentially a VM, but tightly integrated so it runs without the traditional high overhead costs of one. You can run multiple distros/versions simultaneously, each one having its own Virtual Hard Disk (VHD) file.

While WSL2 naturally can't quite compare to a native Linux install, it's quickly becoming a very viable solution for a full development environment on Windows that's able to make use of standard Linux tools and conveniences more fully than MinGW32, Cygwin, etc. It is aiming for full system call compatibility (meaning it can run complex and low-level services like Docker), and convenient interop (e.g. sharing files and networking).

In order to run WSL2, you need the Windows 10 May 2020 Update (version 2004, build 19041). Once WSL2 itself is enabled (see instructions), you can install a distro of your choice - but this project uses `apt` and has only been tested with the latest version of Ubuntu. The easiest way to install Ubuntu is actually from the Microsoft Store (it is published there by Canonical, owners of Ubuntu). For other distros, this project could, at most, serve as a general guideline to configuration you might want to set up.

Side note: While you're in the Microsoft Store, feel free to try out the "Microsoft Terminal" - a free and open-source terminal program that works nicely with cmd, PowerShell, *and* WSL, with colour support, and tabs/panes. It's still new, so it's not super feature-rich yet, but it's worth looking at.

## WSL2 general notes and caveats

- All your Windows drives are automatically mounted into your distro at `/mnt/<letter>`, which is nice. On top of that, you can launch Windows programs from within your Ubuntu context. For example, you could `tail` a Windows log file, or run `/mnt/c/Windows/notepad.exe` to start up Notepad so you can do some serious coding. However: note that file access through these mount points is slower than the VHD, so it's ideal to keep your work in your Linux filesystem when possible.
- You can also choose to mount your WSL2 filesystem as a network drive in Windows. The UNC path is `\\wsl$\Ubuntu-20.04` (or whatever your distro name is).
- VS Code has a particularly good extension, [Remote - WSL](https://code.visualstudio.com/docs/remote/wsl), for opening your WSL2 projects in Windows. You can open a project in your distro either from a new instance, or from within your WSL2 itself. Note that the instructions will say to run `code .` to open VS Code from within WSL, but I've aliased it to `vscode` instead, you can change it back after the setup if you like. All the files and command execution will be in WSL2, but you open the GUI comfortably on Windows. You can even drag and drop files straight into the filesystem with it!
- WSL2 uses a network with NAT that will be on a different IP range than your host machine's LAN. However, by default, traffic to `localhost` will be forwarded to WSL2 automatically, e.g. if you have a web or DB server installed in the distro. Curiously, it only works for `localhost`, not any other hostname even if it resolves to `127.0.0.1`/`::1`.
- There is no `systemd` or `init.d` or whatever the cool distros are using these days. By default, there is absolutely no way to start programs on startup. We will work around that.
- If you find that any SSH connections you open from within WSL2 close themselves after a period of inactivity, you can add `ServerAliveInterval 120` to the top of your `~/.ssh/config` file.
- Personally, performance and capabilities have been pretty good so far, except for a couple of very specific performance issues most people wouldn't run into. And of course, there are the minor annoyances for which I have created workarounds here.

## Let's set it up!

I'll keep the instructions brief here, but you can read on first if you want to understand a little more about what's being done.

1. Follow [these instructions](https://docs.microsoft.com/en-us/windows/wsl/install-win10) to install WSL2. Choose the latest version of Ubuntu.
1. Clone this repo into your WSL2 instance and `cd` into it.
1. Run `./setup.sh`.
1. Use vim or nano to edit `/usr/local/bin/startup` (requires sudo/root) to suit your own needs.
1. Open Windows Task Scheduler and Create Task.
    - Make sure "Hidden" is checked.
    - The Trigger should be "At log on" for "Any user".
    - The Action should be "Start a program", and that program should be `wsl`, with the arguments `sudo startup`.
1. Optionally, read the TCP port proxy section if `localhost` is not enough for you.
1. Run `./limit-memory.sh` and enter reasonable values for your system.
1. From a command prompt outside of WSL2, run `wsl --shutdown` to shut down the WSL VM host
1. Start a new bash instance (which will start the VM host again, with updated config) and run `sudo startup`
1. All done! Files have been copied to your system, so you can remove this repo.

## So what's in this "light starter kit"?

### **WSL conf**

As mentioned, your Windows drives are automatically mounted. And as mentioned, access to them is slower. Under the default setup, your Windows `PATH` is also appended to your Linux `PATH`, which means when you try to do tab completion for a command, it will search a bunch of Windows folders, and tab completion becomes *painfully* slow. So the config here turns that off.

In addition, by default, WSL2 will create the `/etc/hosts` file when it starts up, based on your Windows hosts file. Unless you *actively* use your Windows hosts file, there's no point in recreating that file every startup, so the config here turns that off too.

There is also [an ongoing issue](https://github.com/microsoft/WSL/issues/4166) that allows your WSL2 to use up way too much of your RAM by default. The `limit-memory.sh` step allows you to set a limit to how much it uses, appropriately for your system. This config is stored in a file on the host system, while the rest of it is stored in WSL's filesystem.

### **Startup script**

The startup script works around the lack of `systemd`. The setup will copy a sample script to `/usr/local/bin/startup`, which is on your `PATH`. It will also set up a file in `sudoers.d` so that your Linux user is allowed to run that command, and only that command, with *passwordless* `sudo`.

That means that from Windows, you can run `wsl sudo startup` without a password, which makes it suitable for adding to Windows Scheduled Tasks, as specified in the instructions. Now when your Windows machine starts up, it will run that script, so you can start up your Docker daemon, your cron daemon, Apache, nginx, MySQL, and/or whatever else you need.

As an alternative to the logon task, or if you need to completely shut down and restart your WSL2 distro while logged on: you can run `sudo startup` (again, passwordless) from anywhere in your WSL2 distro. You can put it in your `.bashrc` instead of Windows Scheduled Tasks if you don't want those services to start when you start Windows, but do want them to start when you open a WSL2 terminal.

### **Embiggen your .bashrc**

The setup script will copy a shell script to be a hidden file in your home directory (it will be `~/.wsl.bashrc`). It will append a line to your actual `.bashrc` to load this one at the end, so that it's basically a `.bashrc` add-on.

One of the biggest conveniences I missed early on while working with WSL2 was the `ssh-agent`. On a native Ubuntu install it's always there and ready for you to `ssh-add`, but not so in WSL. This script includes a function that will check if an `ssh-agent` is running, run one if it's not already, and ensure that its context is added to the environment of your shell. In short, it will always be there for you, just like on a native install.

As mentioned earlier, the default setup is to add the Windows `PATH` to the Linux `PATH`, but we are turning that off because it's slow. However, we do lose out on the few Windows programs you would actually want to launch from within WSL2. Aliases to the rescue! They can just point directly to the Windows executable. Two are included (although of course you can add any you like). We have `vscode` to launch VS Code (as the `Remote - WSL` extension will instruct you), and `copy`, which runs windows `clip.exe`, so you can pipe the output of commands straight to your Windows clipboard (e.g. `cat setup.sh | copy`).

And finally, the default location when starting up a new WSL2 Bash instance is your Windows home directory. As mentioned, mounted files are slower, so I think your Linux home directory is a better default. The script will move you there.

### **Notifications**

Another thing I missed from native Ubuntu was the ability to send notifications straight from the command line... but we can use a combination of Bash and Powershell for that functionality here. The setup will copy a Powershell script to your C: drive, and set up a Bash command to call it directly.

The first thing this adds is `notify <info|warning|error> <title> [text]`. This is a straightforward command that can be called system-wide, which will send a title (and optional second line of text) with an info, warning, or error icon. Use it in a script, cron job, or whatever you need.

The next addition is `nac [text]`, short for "notify after command". It checks the exit status of the previous command and sends a Windows notification based on it, which can be useful for letting you know when long-running commands have finished. It relies on the exit code, so it needs to be a bash function, to keep its context. That means it's only accessible in contexts where your .bashrc has been sourced, so no cron jobs (just use `notify` directly).

For `nac`, the usage is a little different if you're not super familiar with Linux: if you want to notify when a command is finished, add `; nac` to the end, or if you only want to notify on failure, add ` || nac` to the end. For example: `netstat -a; nac`. By default this will just send "Command Successful" or "Command Failed" as a notification, but you can add an optional message to it to differentiate if you're running multiple commands, like so: `./long-task.sh || nac 'Long Task'`

Disclaimer: for the Windows notifications, I found more-or-less the same script using WinForms to get a notification from Powershell. It seems like by far the easiest way to accomplish the task, but has a small drawback: an icon will appear in the notification area, and for me at least, does not go away even after you've dismissed the notification. You need to mouse over it, and then it will disappear.

### **TCP port proxy**

By default, traffic to `localhost` is actually configured to go to the WSL2 distro. For example, if you have Apache running in WSL2, and type `localhost` in your browser in Windows, you will see the results just as if Apache were running on Windows. It's great! But it doesn't go far enough for me.

You will not be able to access WSL2 via `localhost` from another machine on your network (including your phone for mobile testing), nor will you be able to access it via any hostname besides `localhost` (if you run real or virtual domains locally), or even directly by your machine's IP address. You *can* actually access it by pointing a domain to the IP address of your WSL2 instance, but it's on a different subnet and also uses DHCP to get a new IP every startup.

As far as I can tell from experimentation and reading GitHub Issue threads, it is not possible to fix, and the best solution is a workaround - a TCP port proxy (a program that runs on your Windows machine listening for TCP connections on a port, and forwards them to the IP address of the WSL2 distro). Fortunately for everyone but me, there is a port proxy built-in to Windows via the command `netsh`. Unfortunately for me, it works great for about half an hour and then stops working.

If you would like to try the `netsh` solution, [this comment](https://github.com/microsoft/WSL/issues/4150#issuecomment-504209723) on the WSL repo has the instructions you'll need. I recommend trying the `netsh` command from the PowerShell script first, to see if it works for you, before automating it.

However, with `netsh` failing for me, the fastest option was actually to write my own quick-and-dirty TCP port proxy. I wrote it in Node.js, because I didn't want to spend too much time on it, but that does mean you'll need Node.js installed in Windows (even if your development environment is in WSL2) if you want to use this solution.

Running it is simple: `node wsl2-proxy.js 80,443` (where 80 and 443 are port numbers you want to forward to WSL2 - add as many as you like, of course). It will actually check your WSL's current IP address, and then start listening on the specified ports and forward them to that IP. So again, if you do a full shutdown of your distro, you'll need to stop and restart the proxy script to find the new IP.

Of course, you don't want to have to run the proxy script manually and keep a console window with it open all the time, right? You could set it as another Scheduled Task on Logon, but check this out: [NSSM](https://nssm.cc), the Non-Sucking Service Manager. It's a program that allows you to set up any command as an actual Windows Service, and it really doesn't suck. Having the proxy as a Windows Service will allow it to start on startup of course, but will also restart it if the process fails, and you can choose to redirect STDOUT (console output) to a log file. Make sure to set it to run under your own account, not as Local Service.

## Have fun!

Any issues with this setup or with these instructions? Annoyed by something in WSL2 that this doesn't address? You know where to find me.


------

*Icon from [freeicons.io](https://www.freeicons.io/free-setting-and-configuration-icons/working-tools-icon-9651)*