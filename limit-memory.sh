#!/usr/bin/env bash

# get user input for values if they weren't provided as args
if [[ -z "$1" ]]; then
    echo -e '\033[1;36mWhat is your Windows username?\033[0m'
    read winUser
else
    winUser="$1"
fi

if [[ -z "$2" ]]; then
    echo -e '\033[1;36mHow much RAM should WSL2 be allowed to use? Include units please, e.g. 8GB:\033[0m'
    read memLimit
else
    memLimit="$2"
fi
if [[ ! "$memLimit" =~ ^[0-9.]+[MG]B$ ]]; then
    echo -e '\033[1;31mPlease specify size with units\033[0m'
    exit 1
fi

if [[ -z "$3" ]]; then
    echo -e "\033[1;36mWhat size swap file would you like? If you're unfamiliar with swap, set it to half of the memory value:\033[0m"
    read swapSize
else
    swapSize="$3"
fi
if [[ ! "$swapSize" =~ ^[0-9.]+[MG]B$ ]]; then
    echo -e '\033[1;31mPlease specify size with units\033[0m'
    exit 1
fi

# generate and write the file
filePath="/mnt/c/Users/$winUser/.wslconfig"
echo -ne "[wsl2]\r\nmemory=$memLimit\r\nswap=$swapSize\r\n" > "$filePath"

# show next step
echo -e "\033[1;32mConfig file written\033[0m"
echo -e "Run \`wsl --shutdown\` (from outside WSL) to shutdown the entire VM host for the config to be updated"
echo -e "Then within a new bash instance, you can verify the settings with \`free -h\`"
